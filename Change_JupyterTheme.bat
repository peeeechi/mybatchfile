@echo off
echo Jupyter Notebook のテーマを下記から選択してください

echo ----- Themes ----- 
echo ""
echo chesterish
echo grade3
echo gruvboxd
echo gruvboxl
echo monokai
echo oceans16
echo onedork
echo solarizedd
echo solarizedl

@set /p theme=




echo %theme% へ変更します
cd C:\Users\1247065\AppData\Miniconda3_4.5.1_64bit\envs\for_web\Scripts
jt -t %theme% -T -N

pause

exit /b