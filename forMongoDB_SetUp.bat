REM ディレクトリ作成
set mongoFolder=C:\MongoDB
set dataFolder=%mongoFolder%\Data
set logFolder=%mongoFolder%\Logs
set configFile=%mongoFolder%\mongod.cfg
mkdir %mongoFolder%
mkdir %dataFolder%
mkdir %logFolder%

REM コンフィグファイルの作成
echo systemLog: > %configFile%
echo     destination: file >> %configFile%
echo     path: C:\MongoDB\Logs\mongod.log >> %configFile%
echo storage: >> %configFile%
echo     dbPath: C:\MongoDB\Data >> %configFile%
echo     engine: mmapv1 >> %configFile%
echo net: >> %configFile%
echo    bindIp: 0.0.0.0 >> %configFile%
echo    port: 27017 >> %configFile%


SETX PATH "%PATH%;C:\Program Files (x86)\MongoDB\Server\3.2\bin" /M


REM Windowsサービスへ登録
"C:\Program Files (x86)\MongoDB\Server\3.2\bin\mongod.exe" --config %configFile% --install --serviceName MongoDB

pause
exit /b

REM set copyFolder="./Test/Copy"
REM mkdir %copyFolder%
REM xcopy /D /S /E /H /C /Y /R "./Copy" %copyFolder%