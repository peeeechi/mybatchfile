@echo off
cd /d %~dp0

set publisher=xabikos
set extensionname=JavaScriptSnippets
set version=1.5.0
echo https://%publisher%.gallery.vsassets.io/_apis/public/gallery/publisher/%publisher%/extension/%extensionname%/%version%/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage > url.txt
start https://%publisher%.gallery.vsassets.io/_apis/public/gallery/publisher/%publisher%/extension/%extensionname%/%version%/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage