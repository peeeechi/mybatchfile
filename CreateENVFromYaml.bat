@echo off
echo 環境を構築するMinicondaのScriptsへのPathを入力してください

set /p envPath=
echo cd %envPath%
echo.
cd %envPath%


echo 環境を構築するYamlファイルへのPathを入力してください

set /p yamlPath=
echo Yamlファイル: %yamlPath%
echo.

conda env create -f=%yamlPath%

pause
exit /b
