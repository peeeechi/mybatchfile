@echo off
echo クローンするMinicondaのScriptsへのPathを入力してください

set /p envPath=
echo cd %envPath%
echo.
cd %envPath%

echo クローンするMinicondaENV名を入力してください
echo -----  Conda ENV 一覧  -----
echo.
conda info -e
echo.

set /p envName=
echo 選択環境名: %envName%

conda env export -n %envName% > %envName%.yaml

pause
exit /b